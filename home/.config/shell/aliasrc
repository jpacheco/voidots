#!/bin/sh

# Use neovim for vim if present.
[ -x "$(command -v nvim)" ] && alias vim="nvim" vimdiff="nvim -d"

# Use $XINITRC variable if file exists.
[ -f "$XINITRC" ] && alias startx="startx $XINITRC"

# sudo not required for some system commands
for command in mount umount sv updatedb su ; do
	alias $command="sudo -A $command"
done; unset command

# Verbosity and settings that you pretty much just always are going to want.
alias \
  sudo="sudo -A" \
	cp="cp -iv" \
	mv="mv -iv" \
	rm="rm -vI" \
	bc="bc -ql" \
	mkd="mkdir -pv" \
	yt="youtube-dl --add-metadata -i" \
	yta="yt -x -f bestaudio/best" \
  yv="ytfzf -t" \
  t="tmux" \
  ta="tmux a" \
	ffmpeg="ffmpeg -hide_banner" \
	cls="clear; ls"  

# Colorize commands when possible.
alias \
	ls="ls --color" \
	lsa="ls -a --color" \
	lsl="ls -l --color" \
	lsla="ls -la --color" \
	grep="grep --color=auto" \
	diff="diff --color=auto" \
	ccat="highlight --out-format=ansi"

# These common commands are just too long! Abbreviate them.
alias \
	ka="killall -9" \
  mpv="devour mpv" \
  sxiv="devour sxiv" \
	cat="bat" \
  smci="sudo make clean install" \
  smi="sudo make install" \
  mi="make install" \
  smcu="sudo make clean uninstall" \
  smu="sudo make uninstall" \
  mi="make install" 

alias \
	g="git" \
	trem="transmission-remote" \
	YT="youtube-viewer" \
	sdn="shutdown -h now" \
	qb="qutebrowser" \
	v="$EDITOR" \
	xi="sudo xbps-install" \
	xr="sudo xbps-remove -R" \
	xq="xbps-query" \
	z="devour zathura" \
	pipes="pipes.sh" \
	sx="devour sxiv" \
  arcom="arduino-cli compile --fqbn arduino:avr:uno" \
  arup="arduino-cli upload -p /dev/ttyUSB0 --fqbn arduino:avr:uno"


cwm(){ du -ax ~/.local/src/dwm-jp | awk '{ print $2 }' | fzf-tmux -p | xargs -r nvim }
ecf(){ du -ax ~/.dotfiles/home/.config/ | awk '{ print $2 }' | fzf-tmux -p | xargs -r nvim }
esc(){ du -ax ~/.dotfiles/home/.local/bin/ | awk '{ print $2 }' | fzf-tmux -p | xargs -r nvim }
